const Router = require('express');
const router = Router();

const { 
    postImages, putImage, deleteImage
} = require('../controllers/images');

router.post('/', postImages);
router.put('/:uid', putImage);
router.delete('/:uid', deleteImage);

/* router.get('/', getUsers);

router.get('/:id', getUser); */

module.exports = router;