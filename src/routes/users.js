const Router = require('express');
const router = Router();

const { 
    getUsers, 
    postUsers, 
    putUsers, 
    deleteUsers, 
    getUser
} = require('../controllers/users');

router.get('/', getUsers);

router.get('/:id', getUser);

router.post('/', postUsers);

router.put('/:id', putUsers);

router.delete('/:id', deleteUsers);

module.exports = router;