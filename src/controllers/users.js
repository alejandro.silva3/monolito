const { v4: uuidv4 } = require('uuid');

const {User, Image} = require('../models/associations');
const imageSchema = require('../models/imageSchema');

const getUsers = async(req, res) => {
    try {
        
        const users = await User.findAll({
            include: {
                model: Image,
                attributes: ['mongoId'],
                as: 'img'
            }
        });

        const usersWithImages = [];

        await Promise.all(
            users.map(async(u) => {

                let img = {
                    image: null
                };
                if (u.img) {
                    img = await imageSchema.findById(u.img.mongoId);   
                }

                usersWithImages.push({
                    ...u.dataValues, 
                    img: img.image
                });
            })
        );
        
        res.json({users: usersWithImages});

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        });
    }    
}

const getUser = async(req, res) => {

    const {id} = req.params;
    try {
        
        const user = await User.findByPk(id,{
            include: {
                model: Image,
                attributes: ['mongoId'],
                as: 'img'
            }
        });

        if (!user) {
            return res.status(404).json({
                msg: `No existe un usuario con el id ${id}`
            })
        }

        let img ={
            image: null
        };
        if (user.img) {
            img = await imageSchema.findById(user.img.mongoId);   
        }

        res.json({
            user: {
                ...user.dataValues, 
                img: img.image
            }
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        });
    }    
}

const postUsers = async(req, res) => {

    const {body} = req;
    try {
        const existEmail = await User.findOne({
            where: {
                email: body.email
            }
        });

        if (existEmail) {
            return res.status(400).json({
                msg: `Ya existe un usuario con el email ${body.email}`
            })
        }

        body.id = uuidv4();

        const user = new User(body);
        await user.save();

        res.json(user);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        })
    }

}

const putUsers = async(req, res) => {

    const {id} = req.params;
    const {body} = req;
    try {
        const user = await User.findByPk(id);

        if (!user) {
            return res.status(404).json({
                msg: `No existe un usuario con el id ${id}`
            })
        }

        const existEmail = await User.findOne({
            where: {
                email: body.email
            }
        });

        if (existEmail) {
            return res.status(400).json({
                msg: `Ya existe un usuario con el email ${body.email}`
            })
        }

        await user.update(body);

        res.json(user);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        })
    }
}

const deleteUsers = async(req, res) => {

    const {id} = req.params;

    try {

        const user = await User.findByPk(id);

        if (!user) {
            return res.status(404).json({
                msg: `No existe un usuario con el id ${id}`
            })
        }

        //await user.destroy();
        await user.update({state: 0});

        res.json(user);
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Comuniquese con el administrador'
        })
    }
}

module.exports = {
    getUsers,
    getUser,
    postUsers,
    putUsers,
    deleteUsers
}