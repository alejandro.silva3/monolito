const User = require('./user');
const Image = require('./image');

User.hasOne(Image, {
    foreignKey: 'uid',
    as: 'img'
});
Image.belongsTo(User);

module.exports = {
    User,
    Image
}