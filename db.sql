CREATE DATABASE `users`;
USE `users`;

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL,
  `name` varchar(70) NOT NULL,
  `age` int NOT NULL,
  `email` varchar(80) NOT NULL,
  `state` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);

CREATE TABLE `retobd`.`images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` VARCHAR(255) NOT NULL,
  `mongoId` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_userimage` FOREIGN KEY (`uid`)
    REFERENCES `users`(`id`)
);

ALTER TABLE `retobd`.`images` 
ADD UNIQUE INDEX `uid_UNIQUE` (`uid` ASC) VISIBLE;
;