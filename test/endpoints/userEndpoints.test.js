const request = require('supertest');

const Server = require('../../src/models/server');
const server = new Server();
const app = server.app; 

describe('Pruebas en endpoint POST - users', () => {
    test('Debe devolver un codigo de status 200 y un objeto con el usuario si es creado', async()=> {
        const user = await request(app).post('/api/users').send({
            name: 'Homero Simpson4',
            age: 12,
            email: 'homerosimpson5@correo.com'
        })
        
        expect(user.statusCode).toBe(200);
        expect(user.body).toBeInstanceOf(Object);
    })

    test('Debe devolver un codigo de status 400 si el usuario ya existe', async()=> {
        const user = await request(app).post('/api/users').send({
            name: 'Homero Doe',
            age: 12,
            email: 'homerodoe@correo.com'
        })
        
        expect(user.statusCode).toBe(400);
    })
})